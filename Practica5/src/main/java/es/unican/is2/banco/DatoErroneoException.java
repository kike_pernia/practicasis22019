package es.unican.is2.banco;

public class DatoErroneoException extends RuntimeException {
	
	public DatoErroneoException (String mensaje) {
		super(mensaje);
	}

}
