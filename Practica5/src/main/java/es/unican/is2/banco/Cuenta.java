package es.unican.is2.banco;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;
public class Cuenta {

	private String mNumero;
	private Cliente cliente;
	private List<Movimiento> mMovimientos;
	private Date mFechaDeCaducidadTarjetaDebito;
	private Date mFechaDeCaducidadTarjetaCredito;
	private double limiteDebito;
	
	public Cuenta(String mNumero, String mTitular, String mDireccion,
			String mtelefono, String mDNI, Date date, Date date2) {

		this.mNumero = mNumero;
		this.cliente = new Cliente(mTitular, mDireccion, mtelefono, mDNI);
		this.mFechaDeCaducidadTarjetaDebito=date;
		this.mFechaDeCaducidadTarjetaCredito=date2;
		mMovimientos=new LinkedList<Movimiento>();
		limiteDebito = 1000;
	}


	public void ingresar(double x) throws DatoErroneoException {

		cantidadNegativaException(x);
		String newMConcepto = "Ingreso en efectivo";
		Movimiento m = generaMovimiento(x, newMConcepto);
		this.mMovimientos.add(m);
	}


	public void cantidadNegativaException(double x) {
		if (x <= 0)
			throw new DatoErroneoException("Cantidad negativa no permitida");
	}


	public Movimiento generaMovimiento(double x, String newMConcepto) {
		Movimiento m = new Movimiento();
		Date d = new Date();
		m.setF(d);
		m.setC(newMConcepto);
		m.setI(x);
		return m;
	}

	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		cantidadNegativaException(x);
		saldoInsufucienteException(x);
		String newMConcepto = "Retirada de efectivo";
		Movimiento m = generaMovimiento(-x, newMConcepto);
		this.mMovimientos.add(m);
	}


	private void saldoInsufucienteException(double x) {
		if (getSaldo() < x)
			throw new SaldoInsuficienteException("Saldo insuficiente");
	}

	
	public void ingresar(String concepto, double x) throws DatoErroneoException {
		cantidadNegativaException(x);
		Movimiento m = generaMovimiento(x, concepto);
		this.mMovimientos.add(m); 
	}

	public void retirar(String concepto, double x) throws SaldoInsuficienteException, DatoErroneoException {
		saldoInsufucienteException(x);
		cantidadNegativaException(x);
		Movimiento m = generaMovimiento(-x, concepto);
		this.mMovimientos.add(m);


	}

	
	public double getSaldo() {
		double r = 0.0;
		for (int i = 0; i < this.mMovimientos.size(); i++) {
			Movimiento m = (Movimiento) mMovimientos.get(i);
			r += m.getI();
		}
		return r;
	}

	public void addMovimiento(Movimiento m) {
		mMovimientos.add(m);
	}

	public List<Movimiento> getMovimientos() {
		return mMovimientos;
	}
	
	public Date getCaducidadDebito(){
		return this.mFechaDeCaducidadTarjetaDebito;
	}
	
	public Date getCaducidadCredito(){
		return this.mFechaDeCaducidadTarjetaCredito;
	}


	public double getLimiteDebito() {
		return limiteDebito;
	}

}