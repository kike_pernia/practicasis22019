package es.unican.is2.banco;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Credito extends Tarjeta {
	
	private static final double COMISION = 0.05;
	private double mCredito;
	private List<Movimiento> historicoMovimientos;
	protected List<Movimiento> mMovimientosMensuales;
	
	//private double saldo;

	public Credito(String numero, String titular, double credito) {
		super(numero, titular);
		mCredito = credito;
		mMovimientosMensuales = new ArrayList<Movimiento>();
		historicoMovimientos = new ArrayList<Movimiento>();
		//saldo = credito;
	}

	@Override
	public void ingresar(double x) throws DatoErroneoException{
		this.mCuentaAsociada.cantidadNegativaException(x);
		
		String newMConcepto = "Ingreso en cajero autom�tico";
		Movimiento m = this.mCuentaAsociada.generaMovimiento(x, newMConcepto);
		mMovimientosMensuales.add(m);
	}

	@Override
	public void retirar(double x) throws SaldoInsuficienteException, DatoErroneoException {
		this.mCuentaAsociada.cantidadNegativaException(x);
		
		String newMConcepto = "Retirada en cajero autom�tico";
		x += x * COMISION; // A�adimos una comisi�n de un 5%
		Movimiento m = this.mCuentaAsociada.generaMovimiento(-x, newMConcepto);
		
		if (x > getSaldo()+mCredito)
			throw new SaldoInsuficienteException("Cr�dito insuficiente");
		else {
			mMovimientosMensuales.add(m);
		}
	}

	@Override
	public void pagoEnEstablecimiento(String datos, double x) throws SaldoInsuficienteException, DatoErroneoException {
		this.mCuentaAsociada.cantidadNegativaException(x);

		if (x > getSaldo()+mCredito)
			throw new SaldoInsuficienteException("Saldo insuficiente");
		
		String newMConcepto = "Compra a cr�dito en: " + datos;
		Movimiento m = this.mCuentaAsociada.generaMovimiento(-x, newMConcepto);
		mMovimientosMensuales.add(m);
		//saldo = saldo -x;
	}
	
	@Override
	public double getSaldo() {
		double saldo = 0.0;
		for (int i = 0; i < this.mMovimientosMensuales.size(); i++) {
			Movimiento m = (Movimiento) mMovimientosMensuales.get(i);
			saldo += m.getI();
		}
		return saldo;
	}
	
	
	public Date getCaducidadCredito() {
		return this.mCuentaAsociada.getCaducidadCredito();
	}

	/**
	 * M�todo que se invoca autom�ticamente el d�a 1 de cada mes
	 */
	public void liquidar() {
		Movimiento liq = new Movimiento();
		liq.setC("Liquidaci�n de operaciones tarj. cr�dito");
		double saldo = getSaldo();
		liq.setI(saldo);

		if (saldo != 0)
			mCuentaAsociada.addMovimiento(liq);
		
		historicoMovimientos.addAll(mMovimientosMensuales);
		mMovimientosMensuales.clear();
	}
	
	public Cuenta getCuentaAsociada() {
		return mCuentaAsociada;
	}
	
	public List<Movimiento> getMovimientos() {
		return mMovimientosMensuales;
	}

}