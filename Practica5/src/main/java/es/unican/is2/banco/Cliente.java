package es.unican.is2.banco;

public class Cliente {
	public String titular;
	public String direccion;
	public String telefono;
	public String dni;

	public Cliente(String titular, String direccion, String telefono, String dni) {
		this.titular = titular;
		this.direccion = direccion; 
		this.telefono = telefono;
		this.dni = dni;
	}
}