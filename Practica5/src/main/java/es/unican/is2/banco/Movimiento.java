package es.unican.is2.banco;

import java.util.Date;

public class Movimiento {
	private String mConcepto;
	private Date mFecha;
	private double mImporte;

	public Movimiento() {
		mFecha = new Date(); 
	}

	public double getI() {
		return mImporte;
	}

	public String getC() {
		return mConcepto;
	}

	public void setC(String newMConcepto) {
		mConcepto = newMConcepto;
	}

	public Date getF() {
		return mFecha;
	}

	public void setF(Date newMFecha) {
		mFecha = newMFecha;
	}

	public void setI(double newMImporte) {
		mImporte = newMImporte;
	}
}