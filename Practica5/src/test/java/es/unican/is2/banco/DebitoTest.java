package es.unican.is2.banco;

import static org.junit.Assert.*;
import java.util.Date;
import org.junit.BeforeClass;
import org.junit.Test;

import es.unican.is2.banco.*;

public class DebitoTest {
	static Debito tDebito;
	static Cuenta cuenta;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		cuenta = new Cuenta("794311", "Juan Gomez","Torrelavega", "942589674","874214563X",new Date(2015,12,12), new Date(2015,12,12));
	}

	@Test 
	public void test1() {

		tDebito = new Debito("5468416521", "Marcos Garc�a", new Date());
		tDebito.setCuenta(cuenta);

		// ingresamos
		try {
			tDebito.ingresar(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getC()
				.equals("Ingreso en cajero autom�tico"));
		assertTrue(tDebito.getSaldo() == 100);

		// retiramos
		try {
			tDebito.retirar(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getC()
				.equals("Retirada en cajero autom�tico"));
		assertTrue(tDebito.getSaldo() == 50);

		// pago desde establecimiento
		try {
			tDebito.pagoEnEstablecimiento("Prueba1", 50);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(cuenta.getMovimientos()
				.get(cuenta.getMovimientos().size() - 1).getC()
				.equals("Compra en : Prueba1"));
		assertTrue(tDebito.getSaldo() == 0);
		// tDebito.addMovimiento(cuenta);

	}

	@Test
	public void test2() {
		try {
			cuenta.ingresar(-20);
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage().equals(
					"Cantidad negativa no permitida"));
		}
	}// test2
}// EscenarioBTest
