package es.unican.is2.Practica3a;

public class Encendido extends ControladorState{

	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#OnOff(es.unican.is2.Practica3a.Lavavajillas)
	 * Si esta encendido se apaga
	 */
	@Override
	public void OnOff(Lavavajillas context) {
		context.setState(ControladorState.getEstadoApagado());
		context.getState().entryAction(context);
	}
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#ProgramaSeleccionado(es.unican.is2.Practica3a.Lavavajillas, java.lang.Integer)
	 * Selecciona el programa en funcion de la posicion que le pases
	 */
	@Override
	public void ProgramaSeleccionado(Lavavajillas context, int programa) {
		context.setProgramaElegido(programa);
	}

	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#PuertaCerrada(es.unican.is2.Practica3a.Lavavajillas)
	 * Carga el programa y modifica el estado del lavavajillas
	 */
	@Override
	public void PuertaCerrada(Lavavajillas context) {
		if(context.isArrancar()){
			 context.setTiempoFinLavado(context.getProgramaElegido().getTiempo());
			 context.setState(ControladorState.getEstadoLavando());
			 context.getState().entryAction(context);
		}
	}

	
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#Arrancar(es.unican.is2.Practica3a.Lavavajillas)
	 * Arranca el lavavajillas con el programa seleccionado
	 */
	@Override
	public void Arrancar(Lavavajillas context) {
		boolean arrancar= !context.isArrancar();
		 context.setArrancar(arrancar);
		 System.out.println(arrancar);
	}
}
