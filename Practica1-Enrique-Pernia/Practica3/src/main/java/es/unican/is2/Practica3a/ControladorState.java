package es.unican.is2.Practica3a;

import java.util.HashMap;

public abstract class  ControladorState {

	private static Encendido estadoEncendido=new Encendido();
	private static Apagado estadoApagado =new Apagado();
	private static Pausa estadoPausa=new Pausa();
	private static Lavando estadoLavando=new Lavando();
	private static HashMap<Integer, ProgramaLavado> programas;
	private static HashMap<Integer, ControladorLuz> luz;

	
	/*
	 * Método que inicia el programa con el estado apagado
	 */
	public static ControladorState init(Lavavajillas context) {
		estadoApagado.entryAction(context);
		programas = new HashMap<Integer, ProgramaLavado>();
		luz=new HashMap<Integer, ControladorLuz>();
		return estadoApagado;
	}
	
	public void OnOff(Lavavajillas context) {};
	public void Arrancar(Lavavajillas context) {};
	public void ProgramaSeleccionado(Lavavajillas context, int programa) {};
	public void PuertaAbierta(Lavavajillas context) {};
	public void PuertaCerrada(Lavavajillas context) {};
	public void entryAction(Lavavajillas context) {};
	public void doAction(Lavavajillas context) {};
	public void exitAction(Lavavajillas context) {};

	/*
	 * Devuelve los programas del lavavajillas
	 */
	public HashMap<Integer, ProgramaLavado> getProgramas() {
		return programas;
	}
	
	/*
	 * Cambia la lista de programas por una que se pasa como parametro
	 */
	public void setProgramas(HashMap<Integer, ProgramaLavado> programas) {
		this.programas = programas;
	}
	
	/*
	 * Devuelve las luces de cada programa
	 */
	public HashMap<Integer, ControladorLuz> getLucesProgramas() {
		return luz;
	}
	
	/*
	 * Cambia la lista de luces de cada programa
	 */
	public void setLucesProgramas(HashMap<Integer, ControladorLuz> lucesProgramas) {
		this.luz = lucesProgramas;
	}
	
	
	/*
	 * Devuelve el estado apagado
	 */
	public static Apagado getEstadoApagado() {
		return estadoApagado;
	}
	
	/*
	 * Añade un programa a la lista de programas del lavavajillas
	 */
	public static void anahadePrograma(int i, ProgramaLavado p) {
		programas.put(i,p);
	}
	
	public static void anahadePrograma(int i, ControladorLuz p) {
		luz.put(i,p);
	}

	/*
	 * Devuelve el estado encendido
	 */
	public static Encendido getEstadoEncendido() {
		return estadoEncendido;
	}

	/*
	 * Devuelve el programa en la posicion i
	 */
	public static ProgramaLavado getPrograma(int i) {
		return programas.get(i);
	}
	
	/*
	 * Devuelve el estado pausa
	 */
	public static Pausa getEstadoPausa() {
		return estadoPausa;
	}

	/*
	 * Devuelve el estado lavando
	 */
	public static Lavando getEstadoLavando() {
		return estadoLavando;
	}

}
