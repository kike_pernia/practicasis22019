package es.unican.is2.Practica3a;
public class Apagado extends ControladorState{

	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#OnOff(es.unican.is2.Practica3a.Lavavajillas)
	 * Enciende el lavavajillas si esta apagado
	 */
	@Override
	public void OnOff(Lavavajillas context) {
		context.setState(ControladorState.getEstadoEncendido());
		context.getState().entryAction(context);
	}
	
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#entryAction(es.unican.is2.Practica3a.Lavavajillas)
	 * No puede empezar a lavar si no se ha encendido
	 */
	@Override
	public void entryAction(Lavavajillas context) {
		context.setArrancar(false);
	}

}
