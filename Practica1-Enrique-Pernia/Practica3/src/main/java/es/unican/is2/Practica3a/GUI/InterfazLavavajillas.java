package es.unican.is2.Practica3a.GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import es.unican.is2.Practica3a.Lavavajillas;
import es.unican.is2.Practica3a.ProgramaLavado;

public class InterfazLavavajillas {

	private JFrame frame;
	private JPanel panel = new JPanel();
	private JPanel panel_1 = new JPanel();
	private JPanel panel_2 = new JPanel();
	private JPanel panel_3 = new JPanel();
	private JPanel panel_5 = new JPanel();
	private Lavavajillas l=new Lavavajillas();
	private boolean on=false;
	
	private int ultimoPrograma = -1;
	
	JButton btnNewButton = new JButton("ECO");
	JButton btnNewButton_1 = new JButton("RAPIDO");
	JButton btnNewButton_2 = new JButton("INTENSO");
	JButton btnNewButton_3 = new JButton("PRELAVADO");
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazLavavajillas window = new InterfazLavavajillas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazLavavajillas() {
	
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
			
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 333, 342);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		panel.setBounds(88, 105, 31, 14);
		frame.getContentPane().add(panel);
		
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(on){
					l.setProgramaElegido(1);
					panel.setBackground(Color.GREEN);
					panel_1.setBackground(Color.DARK_GRAY);
					panel_2.setBackground(Color.DARK_GRAY);
					panel_3.setBackground(Color.DARK_GRAY);
					ultimoPrograma = 1;
				}
				
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(56, 81, 101, 23);
		frame.getContentPane().add(btnNewButton);
		
		
		panel_1.setBounds(219, 105, 31, 14);
		frame.getContentPane().add(panel_1);
		
		
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(on){
					l.setProgramaElegido(2);
					panel.setBackground(Color.DARK_GRAY);
					panel_1.setBackground(Color.GREEN);
					panel_2.setBackground(Color.DARK_GRAY);
					panel_3.setBackground(Color.DARK_GRAY);
					ultimoPrograma = 2;
				}
				
			}
		});
		btnNewButton_1.setBounds(182, 81, 101, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_4 = new JButton("ARRANCAR");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.out.println("arrancar1: "+l.getState());
				if (on && l.getProgramaElegido()!=null) {
					new Ventana2(l);
					l.Arrancar();
				}
				System.out.println("arrancar2: "+l.getState());
			}
		});
		
		btnNewButton_4.setBounds(113, 230, 101, 57);
		frame.getContentPane().add(btnNewButton_4);
		
		
		panel_2.setBounds(88, 155, 31, 14);
		frame.getContentPane().add(panel_2);
		
		
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(on){
					l.setProgramaElegido(3);
					panel.setBackground(Color.DARK_GRAY);
					panel_1.setBackground(Color.DARK_GRAY);
					panel_2.setBackground(Color.GREEN);
					panel_3.setBackground(Color.DARK_GRAY);
					ultimoPrograma = 3;
				}
				
			}
		});
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_2.setBounds(56, 130, 101, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		panel_3.setBounds(219, 155, 31, 14);
		frame.getContentPane().add(panel_3);
		
		panel.setBackground(Color.DARK_GRAY);
		panel_1.setBackground(Color.DARK_GRAY);
		panel_2.setBackground(Color.DARK_GRAY);
		panel_3.setBackground(Color.DARK_GRAY);
		
		
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(on){
					l.setProgramaElegido(4);
					panel.setBackground(Color.DARK_GRAY);
					panel_1.setBackground(Color.DARK_GRAY);
					panel_2.setBackground(Color.DARK_GRAY);
					panel_3.setBackground(Color.GREEN);
					ultimoPrograma = 4;
				}
				
			}
		});
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setBounds(182, 130, 101, 23);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnOnoff = new JButton("On/Off");
		btnOnoff.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println(l.getState());
				if(on){
					l.getState().OnOff(l);
					
					on=false;
					panel.setBackground(Color.DARK_GRAY);
					panel_1.setBackground(Color.DARK_GRAY);
					panel_2.setBackground(Color.DARK_GRAY);
					panel_3.setBackground(Color.DARK_GRAY);
					panel_5.setBackground(Color.DARK_GRAY);
				}
				else{
					if(ultimoPrograma != -1) {
						System.out.println(ultimoPrograma);
						l.setProgramaElegido(ultimoPrograma);
						switch(ultimoPrograma) {
							case 1:
								l.setProgramaElegido(1);
								panel.setBackground(Color.GREEN);
								panel_1.setBackground(Color.DARK_GRAY);
								panel_2.setBackground(Color.DARK_GRAY);
								panel_3.setBackground(Color.DARK_GRAY);
								break;
							case 2:
								l.setProgramaElegido(2);
								panel.setBackground(Color.DARK_GRAY);
								panel_1.setBackground(Color.GREEN);
								panel_2.setBackground(Color.DARK_GRAY);
								panel_3.setBackground(Color.DARK_GRAY);
								break;
							case 3:
								l.setProgramaElegido(3);
								panel.setBackground(Color.DARK_GRAY);
								panel_1.setBackground(Color.DARK_GRAY);
								panel_2.setBackground(Color.GREEN);
								panel_3.setBackground(Color.DARK_GRAY);
								break;
							case 4:
								l.setProgramaElegido(4);
								panel.setBackground(Color.DARK_GRAY);
								panel_1.setBackground(Color.DARK_GRAY);
								panel_2.setBackground(Color.DARK_GRAY);
								panel_3.setBackground(Color.GREEN);
								break;
						}
					}
					l.getState().OnOff(l);
					on=true;
					panel_5.setBackground(Color.GREEN);
				}
				System.out.println(l.getState());
			}
				
		});
		btnOnoff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOnoff.setBounds(113, 28, 101, 23);
		frame.getContentPane().add(btnOnoff);
		
		panel_5.setBackground(Color.DARK_GRAY);
		panel_5.setBounds(145, 50, 38, 14);
		frame.getContentPane().add(panel_5);
		
		JLabel lblNewLabel = new JLabel("---[CONTROLES]---");
		lblNewLabel.setBounds(113, 11, 101, 14);
		frame.getContentPane().add(lblNewLabel);
	}
}