package es.unican.is2.Practica3a.GUI;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;

import es.unican.is2.Practica3a.ControladorState;
import es.unican.is2.Practica3a.Lavavajillas;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ventana2 {

	private JFrame frame;

	private JPanel panel = new JPanel();
	private JPanel panel_1 = new JPanel();
	private Lavavajillas lavavajillas;
	

	/**
	 * Create the application.
	 * @param l 
	 */
	public Ventana2(Lavavajillas l) {
		this.lavavajillas=l;
		initialize();
		
		frame.setVisible(true);
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 451, 221);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		
		
		
		
		JButton btnPuertaAbierta = new JButton("Abrir Puerta");
		
		btnPuertaAbierta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				lavavajillas.getState().PuertaAbierta(lavavajillas);
								
				panel_1.setBackground(Color.DARK_GRAY);
				panel.setBackground(Color.GREEN);
				
				
			}
		});
		
		btnPuertaAbierta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnPuertaAbierta.setBounds(44, 57, 144, 25);
		frame.getContentPane().add(btnPuertaAbierta);
		
		
		
		JButton btnPuertaCerrada = new JButton("Cerrar puerta");
		
		btnPuertaCerrada.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("cerrar 1: "+lavavajillas.getState());

				lavavajillas.getState().PuertaCerrada(lavavajillas);
				System.out.println(lavavajillas.getState());
				panel_1.setBackground(Color.GREEN);
				panel.setBackground(Color.DARK_GRAY);
				
					
				
				
			}
		});
		
		btnPuertaCerrada.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnPuertaCerrada.setBounds(220, 57, 144, 25);
		frame.getContentPane().add(btnPuertaCerrada);
		
		
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(62, 110, 103, 25);
		frame.getContentPane().add(panel);
		
		panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setBounds(237, 110, 103, 25);
		frame.getContentPane().add(panel_1);
		
		
	}
}
