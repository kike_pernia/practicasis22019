package es.unican.is2.Practica3a;
import java.util.Timer;

public class Lavando extends ControladorState{
	
	private long tiempo ;
	protected Timer timer;
	protected LavadoTerminado lavando;
	
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#PuertaAbierta(es.unican.is2.Practica3a.Lavavajillas)
	 * Si la puerta se abre se pasa al estado pausa y el tiempo de lavado se actualiza
	 */
	@Override
	public void PuertaAbierta(Lavavajillas context) {
		tiempo=System.currentTimeMillis()-tiempo;
		context.setTiempoFinLavado(context.getTiempoFinLavado()-tiempo);//Restamos el tiempo transcurrido al tiempo total
		context.setState(ControladorState.getEstadoPausa());
		context.stop();
		context.getState().entryAction(context);
		timer.cancel();
	}

	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#entryAction(es.unican.is2.Practica3a.Lavavajillas)
	 * Comienza el lavado con un timer del tiempo que queda
	 */
	@Override
	public void entryAction(Lavavajillas context) {
		tiempo =System.currentTimeMillis();
		lavando = new LavadoTerminado(context);
		context.start();
		timer = new Timer();
		timer.schedule(lavando, context.getTiempoFinLavado());
	}

	/*
	 * Devuelve el tiempo de lavado
	 */
	public long getTiempo() {
		return tiempo;
	}
}
