package es.unican.is2.Practica3a;

public class ProgramaLavado {

	private String nombre;
	private int tiempo;
	
	/*
	 * Constructor que crea un nuevo programa con un nombre y tiempo
	 */
	public ProgramaLavado(String nombre, int tiempo) {
		super();
		this.nombre = nombre;
		this.tiempo = tiempo;
	}
	
	/*
	 * Devuelve el tiempo del programa
	 */
	public int getTiempo() {
		return tiempo;
	}
	
	/*
	 * Cambia el tiempo del programa
	 */
	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}
	
	
}
