package es.unican.is2.Practica3a;
import java.util.TimerTask;


public class LavadoTerminado  extends TimerTask{
	
	private Lavavajillas context;
	private long tiempo2=System.currentTimeMillis();
	
	/*
	 * Constructor que crea una task para controlar la cuenta atrás
	 */
	public LavadoTerminado(Lavavajillas ctx) 
	{
		context=ctx;
	}

	/*
	 * (non-Javadoc)
	 * @see java.util.TimerTask#run()
	 * termina el lavado
	 */
	@Override
	public void run() {
		
		if(context.getTiempoFinLavado() +tiempo2 <= System.currentTimeMillis()) {
			this.context.setState(ControladorState.getEstadoApagado());
			this.context.getState().entryAction(context);
//			ControladorState.getEstadoApagado().entryAction(context);
			this.context.termina();
		}
	}
}
