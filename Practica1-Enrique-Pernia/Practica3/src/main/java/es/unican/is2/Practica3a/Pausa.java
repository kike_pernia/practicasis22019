package es.unican.is2.Practica3a;

public class Pausa extends ControladorState{

	private long tiempoEspera;
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#OnOff(es.unican.is2.Practica3a.Lavavajillas)
	 * Apaga el lavavajillas si esta encendido
	 */
	@Override
	public void OnOff(Lavavajillas context) {
		context.setState(ControladorState.getEstadoApagado());
		context.getState().entryAction(context);
	}

	
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#PuertaCerrada(es.unican.is2.Practica3a.Lavavajillas)
	 * Carga el programa y modifica el estado del lavavajillas
	 */
	@Override
	public void PuertaCerrada(Lavavajillas context) {
		context.setState(ControladorState.getEstadoLavando());
		context.getState().entryAction(context);
	}
	
	/*
	 * (non-Javadoc)
	 * @see es.unican.is2.Practica3a.LavavajillasState#entryAction(es.unican.is2.Practica3a.Lavavajillas)
	 */
	@Override
	public void entryAction(Lavavajillas context) {
		tiempoEspera = System.currentTimeMillis();
	}

}
