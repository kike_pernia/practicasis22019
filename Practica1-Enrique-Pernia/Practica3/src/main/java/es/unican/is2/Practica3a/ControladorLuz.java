package es.unican.is2.Practica3a;

public class ControladorLuz {
	
	private boolean encendido = false;

	/*
	 * Enciendo la luz del programa
	 */
	public void on() {
		encendido = true;
	}
	
	/*
	 * Apago la luz del programa
	 */
	public void off() {
		
		encendido = false;
	}
}
