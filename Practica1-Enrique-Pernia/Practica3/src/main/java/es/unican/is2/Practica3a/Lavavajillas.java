package es.unican.is2.Practica3a;
import java.util.HashMap;

public class Lavavajillas {
	private boolean arrancar;
	private long  tiempoFinLavado;
	private ProgramaLavado programaElegido;
	private ControladorState state;
	
	/*
	 * Constructor de la clase lavavajillas, pone los atributos iniciales y crea 
	 * y añade los distintos programas de lavado
	 */
	public Lavavajillas() {
		 programaElegido=null;
		 state=ControladorState.init(this);
		 arrancar=false;
		 tiempoFinLavado=0;
		 ProgramaLavado ECO= new ProgramaLavado("ECO",5000);
		 ProgramaLavado RAPIDO= new ProgramaLavado("RAPIDO",5000);
		 ProgramaLavado INTENSO= new ProgramaLavado("INTENSO",5000);
		 ProgramaLavado PRELAVADO= new ProgramaLavado("PRELAVADO",5000);
		 ControladorState.anahadePrograma(1, ECO);
		 ControladorState.anahadePrograma(2, RAPIDO);
		 ControladorState.anahadePrograma(3, INTENSO);
		 ControladorState.anahadePrograma(4, PRELAVADO);
		 ControladorState.anahadePrograma(1, new ControladorLuz());
		 ControladorState.anahadePrograma(2, new ControladorLuz());
		 ControladorState.anahadePrograma(3, new ControladorLuz());
		 ControladorState.anahadePrograma(4, new ControladorLuz());
		 state=ControladorState.getEstadoApagado();
		 
	}
	
	/*
	 * Comienza el lavado del lavavajillas 
	 */
	public void start() {
		System.out.println("Inicio");
	}
	
	/*
	 * Pausa el lavado del lavavajillas
	 */
	public void stop() {
		System.out.println("Para");
	}
	
	/*
	 * Acaba el lavado del lavavajillas
	 */
	public void termina() {
		System.out.println("Termina");
	}
	
	
	/*
	 * Enciende el lavavajillas si esta apagado
	 */
	public void OnOff() {
		state.OnOff(this);
	}
	
	/*
	 * Cambia el programa seleccionado
	 */	
	public void ProgramaSeleccionado(Integer i) {
		state.ProgramaSeleccionado(this,i);
	}
	
	/*
	 * Cambia el estado a puerta abierta
	 */
	public void PuertaAbierta() {
		state.PuertaAbierta(this);
	}
	
	/*
	 * Cambia el estado a puerta cerrada
	 */
	public void PuertaCerrada() {
		state.PuertaCerrada(this);
	}
	
	/*
	 * Cambia el estado a arrancar
	 */
	public void Arrancar() {
		state.Arrancar(this);
	}
	
	/*
	 * Cambia el estado según el que le pases de parámetro
	 */
	public void setState(ControladorState state) {
		this.state=state;
	}
	
	/*
	 * Devuelve si esta arrancado o no
	 */
	public boolean isArrancar() {
		return arrancar;
	}
	
	/*
	 * Cambia el valor arrancar por el pasado como parámetro
	 */
	public void setArrancar(boolean arrancar) {
		this.arrancar = arrancar;
	}
	
	/*
	 * Devuelve el tiempo de fin de lavado
	 */
	public long getTiempoFinLavado() {
		return tiempoFinLavado;
	}
	
	/*
	 * Establece un tiempo de lavado
	 */
	public void setTiempoFinLavado(long l) {
		this.tiempoFinLavado = l;
	}
	
	/*
	 * Devuelve el programa elegido
	 */
	public ProgramaLavado getProgramaElegido() {
		return programaElegido;
	}
	
	/*
	 * Cambia el programa elegido
	 */
	public void setProgramaElegido(Integer programa) {
		this.programaElegido = ControladorState.getPrograma(programa);
	}
	
	/*
	 * Devuelve el estado actual
	 */
	public ControladorState getState() {
		return state;
	}
	
	
}
